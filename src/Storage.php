<?php

namespace Flood\Component\Config;

/**
 * Class Storage - provides storing and dynamic accessing logic for config distributed config classes
 *
 * This class is used as parent in Flood\Component\Config\Config, which could be used to build config classes among different lib's, components which share the same configuration data.
 *
 * @package Flood\Component\Config
 */
abstract class Storage {

    protected static $data = [];

    protected static $data_access_cache = [];

    /**
     * @param $file
     */
    static public function setEnv($file) {
        if(is_file($file)) {
            $config = file($file);
            foreach($config as $conf) {
                $conf = trim($conf);

                // when `=` on pos '0' it also is an invalid line
                // when `#` on pos '0' it is a comment line
                if(strpos($conf, '=') && 0 !== strpos($conf, '#')) {
                    putenv($conf);
                }
            }
        }
    }

    /**
     * Sets a group of config values with the `name` to the storage
     *
     * @param string $name
     * @param array  $data
     */
    protected function setConfigGroup($name, $data) {
        static::$data = \Flood\Component\Func\Array_::merge_recursive_distinct(
            static::$data,
            [$name => $data]
        );
    }

    /**
     * @param array $key
     *
     * @return null|mixed
     */
    public function getData($key = []) {
        if(!is_array($key)) {
            $key = [$key];
        }
        // concat all submitted keys to use as one index in the cache array
        $key_cache = implode('.', $key);

        // check if the config was already cached and return the cached value
        if(isset(static::$data_access_cache[$key_cache])) {
            return static::$data_access_cache[$key_cache];
        }

        $env_cache = str_replace('-', '_', strtoupper(implode('__', $key)));
        if(getenv($env_cache)) {
            static::$data_access_cache[$key_cache] = getenv($env_cache);
            return static::$data_access_cache[$key_cache];
        }

        // var_dump(static::$data);
        if(isset(static::$data[$key[0]])) {
            $setting_scope = [];
            // zero index is the base setting
            $setting_scope[0] = &static::$data[$key[0]];
            $iteration_key = $key;
            unset($iteration_key[0]);// unset the first key, as this selection was done now
            $iteration_key = array_values($iteration_key);

            if(is_array($setting_scope)) {
                // go through each key
                for($i = 1; $i <= count($iteration_key); $i++) {
                    // and set the next index with the value of the key
                    // {new_scope}[index] = {scope, one index previous}[key for this iteration]
                    $setting_scope[$i] = &$setting_scope[$i - 1][$iteration_key[$i - 1]];
                }
                // result is the latest setting scope
                $result = &$setting_scope[count($setting_scope) - 1];
                // setting with result the access cache
                static::$data_access_cache[$key_cache] = &$result;
            } else {
                $result = null;
            }
        } else {
            $result = null;
        }

        return $result;
    }
}